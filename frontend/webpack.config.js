const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';

    return {
        mode: isProduction ? 'production' : 'development',
        entry: {
            main: './personal-website-master/src/main.js',
            worker: './personal-website-master/src/worker.js',
        },
        output: {
            filename: 'bundle.[contenthash].js',
            path: path.resolve(__dirname, 'dist'),
            clean: true,
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: 'babel-loader',
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.(png|jpg|jpeg|gif)$/i,
                    type: 'asset/resource',
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './personal-website-master/src/index.html',
            }),
            new CopyWebpackPlugin({
                patterns: [
                    { from: './personal-website-master/src/assets', to: 'assets' },
                ],
            }),
            new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
        ],
        optimization: {
            minimizer: isProduction
                ? [
                    new TerserWebpackPlugin({
                        terserOptions: {
                            compress: {
                                drop_console: true,
                            },
                        },
                    }),
                ]
                : [],
        },
        devServer: {
            static: {
                directory: path.resolve(__dirname, 'public'),
            },
            port: 8080,
            proxy: {
                '/subscribe': 'http://localhost:3000', 
            },
        },
    };
};
