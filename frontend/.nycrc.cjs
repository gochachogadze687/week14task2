module.exports = {
  "reporter": ["text", "text-summary"],
  "extension": [".js"],
  "include": ["src/**/*.js"],
  "exclude": ["**/*.test.js", "**/tests/**"],
  "all": true
};

