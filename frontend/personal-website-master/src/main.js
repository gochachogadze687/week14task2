import './email-validator.js';   
import './website-section.js';   
import { setupEmailInput } from './join-us-section.js';
import './styles/style.css';





const joinForm = document.getElementById('joinForm');
const unsubscribeContainer = document.getElementById('unsubscribeContainer');
const unsubscribeButton = document.getElementById('unsubscribeButton');
const worker = new Worker(new URL('./worker.js', import.meta.url));



async function handleUnsubscribe() {
  try {
    unsubscribeButton.disabled = true;
    unsubscribeButton.style.opacity = '0.5';
    const response = await fetch('/unsubscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      emailInput.style.display = 'block';
      unsubscribeContainer.innerHTML = '';
      unsubscribeButton.style.display = 'none'; 
      joinForm.style.display = 'block'; 
      localStorage.removeItem('subscriptionEmail');
    } else {
      const errorMessage = document.createElement('p');
      errorMessage.textContent = 'Failed to unsubscribe: ' + response.statusText;
      errorMessage.style.color = 'red';
      unsubscribeContainer.appendChild(errorMessage);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  } finally {
    unsubscribeButton.disabled = false;
    unsubscribeButton.style.opacity = '1'; 
  }
}


async function handleFormSubmit(e) {
  e.preventDefault();

  try {
    const response = await fetch('/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      hideEmailInputAndShowButton();
      joinForm.style.display = 'none';
    } else if (response.status === 422) {
      const errorData = await response.json();
      const errorContainer = document.getElementById('errorContainer');
      errorContainer.textContent = errorData.error;
      errorContainer.style.display = 'block'; 
    } else {
      console.error('Failed to subscribe:', response.statusText);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}




document.addEventListener('DOMContentLoaded', () => {
  const emailInput = document.getElementById('email'); 

  setupEmailInput();

  const subscribeButton = document.getElementById('subscribe-button');

  if (subscribeButton) {
    subscribeButton.addEventListener('click', () => {
      console.log('Subscribe button clicked');
      worker.postMessage({ type: 'subscribeButtonClicked' });
    });
  }

  if (emailInput) {
    emailInput.addEventListener('input', () => {
    });
  }
});












window.addEventListener('load', () => {
  const loadTime = window.performance.now();
  console.log(`Page load time: ${loadTime.toFixed(2)} milliseconds.`);
});




window.addEventListener('load', () => {
  if (performance && performance.memory) {
    const memoryUsage = performance.memory.usedJSHeapSize / 1024 / 1024; 
    console.log(`Page memory usage: ${memoryUsage} MB.`);
  }
});
















async function logPerformanceMetrics() {
  const loadTime = performance.now() - performance.timeOrigin;
  console.log(`Page load time: ${loadTime.toFixed(2)} milliseconds.`);

  if (performance && performance.memory) {
    const memoryUsage = performance.memory.usedJSHeapSize / 1024 / 1024;
    console.log(`Page memory usage: ${memoryUsage} MB.`);

    const communityStartTime = performance.now();
    try {
      // const communityResponse = await fetch('http://localhost:3000/community');
      // const communityData = await communityResponse.json();
      const communityEndTime = performance.now();
      const communityFetchTime = communityEndTime - communityStartTime;
      console.log(`Community fetch time: ${communityFetchTime} milliseconds`);

      const performanceMetrics = {
        communityPerformance: communityFetchTime,
        pageLoadSpeed: loadTime,
        pageMemoryUsage: memoryUsage,
      };

      const serverUrl = 'http://localhost:3000/analytics/performance';

      try {
        const response = await fetch(serverUrl, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(performanceMetrics),
          mode: 'no-cors',
        });

        console.log('Fetch Response:', response);

        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();
        console.log('Metrics sent successfully:', data);

        localStorage.setItem('measurementsDone', 'true');
      } catch (error) {
        console.error('Error sending metrics:', error);
      }
    } catch (error) {
      console.error('Error fetching community data:', error);
    }
  }
}

logPerformanceMetrics();




