export function setupEmailInput() {
  const emailInput = document.getElementById('email');

  function saveEmailToLocalStorage() {
    const emailValue = emailInput.value;
    localStorage.setItem('subscriptionEmail', emailValue);
  }

  emailInput.addEventListener('input', saveEmailToLocalStorage);

  function populateEmailFromLocalStorage() {
    const storedEmail = localStorage.getItem('subscriptionEmail');
    if (storedEmail) {
      emailInput.value = storedEmail;
    }
  }

  populateEmailFromLocalStorage();
}
