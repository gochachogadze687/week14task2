class WebsiteSection extends HTMLElement {
    constructor() {
        super();

        const shadowRoot = this.attachShadow({ mode: 'open' });

        const template = document.createElement('template');
        template.innerHTML = `
            <style>
                :host {
                    display: block;
                    position: relative;
                    // --background-image: url('../assets/images/your-image-here.jpg');
                    --height: 698px;
                }

                .section {
                    background: linear-gradient(to top, rgba(65, 65, 65, 0), rgba(65, 65, 65, 0)), var(--background-image) no-repeat center/cover;
                    height: 698px;
                    max-width: 100%;
                    color: white;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    flex-direction: column;
                }

                .app-title {
                    font-size: 3.25rem;
                    line-height: 3.20rem;
                    text-align: center;
                }

                .app-subtitle {
                    font-size: 1.50rem;
                    line-height: 1.625rem;
                    font-weight: 300;
                    text-align: center;
                }
            </style>



            
            <div class="section">
                <h1 class="app-title"><slot name="title"></slot></h1>
                <slot name="content"></slot>
                <h2 class="app-subtitle"><slot name="subtitle"></slot></h2>
              
            </div>
        `;

        shadowRoot.appendChild(template.content.cloneNode(true));
    }
}

customElements.define('website-section', WebsiteSection);
