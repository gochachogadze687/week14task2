const batchSize = 5;
const serverUrl = "http://localhost:3000";
let batch = [];


self.addEventListener('message', (event) => {
  batch.push(event.data);

  if (batch.length === batchSize) {
    sendBatchToServer(batch); 
    batch = [];
  }
});




async function sendBatchToServer(batchData) {
  try {
    console.log('Sending batch to server:', batchData);
    const response = await fetch(`${serverUrl}/analytics/user`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(batchData),
      mode: 'no-cors'
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const responseData = await response.json();
    console.log('Response from server:', responseData);
  } catch (error) {
    // console.error('Error sending data batch:', error);
  }
};

